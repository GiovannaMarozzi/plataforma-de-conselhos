package com.example.hypeadvice.domain.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@javax.persistence.Entity
@Table(name = "compra")
public class Compras {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_conselho", nullable = false)
    private Advice conselho;

    @Column(name = "cliente", nullable = false)
    private String cliente;

    @Column(name = "venda")
    private Double venda;

}
