package com.example.hypeadvice.domain.service;


import com.example.hypeadvice.domain.entity.Advice;
import com.example.hypeadvice.domain.entity.Compras;
import com.example.hypeadvice.domain.repository.ComprasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ComprasService {

    @Autowired
    private ComprasRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public void salvarCompra(Advice advice, Compras compras) {
        compras.setConselho(advice);
        compras.setVenda(advice.getPreco());
        repository.save(compras);
    }

    public Integer buscarConselhosPorId(Long id) {
        return repository.countByConselhoId(id);
    }
}
