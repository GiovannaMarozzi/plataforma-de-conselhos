package com.example.hypeadvice.domain.service;

import com.example.hypeadvice.domain.entity.Advice;
import com.example.hypeadvice.domain.repository.AdviceRepository;
import com.example.hypeadvice.domain.repository.ComprasRepository;
import com.example.hypeadvice.domain.vo.AdviceListVO;
import com.example.hypeadvice.domain.vo.AdviceVO;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

@Service
public class AdviceService {

    @Autowired public AdviceRepository adviceRepository;
    @Autowired public AdvicesLIPService advicesLIPService;

    @Autowired public ComprasRepository comprasRepository;

    @Transactional(rollbackFor = Exception.class)
    public Advice save(Advice analiseContrato) {
        return adviceRepository.saveAndFlush(analiseContrato);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Advice> findAll() {
        return adviceRepository.findAll();
    }

    public Advice gerar() throws UnirestException {
        return advicesLIPService.gerar();
    }

    public AdviceListVO buscar(Advice advice) throws UnirestException {
      String descricao = advice.getDescricao();
      if (StringUtils.isNotBlank(descricao)) {
          return advicesLIPService.buscarByDescricao(descricao);
      }
      return null;
    }

    @Transactional
    public void excluir(Long id){
        adviceRepository.deleteAllById(Collections.singleton(id));
    }

    @Transactional
    public void atualizar(Advice advice) {
        adviceRepository.salvarEdicao(advice.getId(), advice.getNome(), advice.getDescricao(), advice.getTipo(), advice.getPreco());
    }

    public AdviceVO buscarPorId(Long id) throws UnirestException {
        if (id != null) {
            return advicesLIPService.buscarById(id);
        }
        return null;
    }

    public AdviceListVO listarTodos() throws UnirestException {
        return advicesLIPService.buscarTodosOsConselhos();
    }

    @Transactional
    public void atualizarQtd(Advice advice) {
        int qtdCompras = comprasRepository.countByConselhoId(advice.getId());
        adviceRepository.adicionarCompra(qtdCompras++, advice.getId());
    }
}
