package com.example.hypeadvice.domain.repository;

import com.example.hypeadvice.domain.entity.Advice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdviceRepository extends JpaRepository<Advice, Long> {

    @Modifying
    @Query("UPDATE Advice a SET a.nome = :nome, a.descricao = :descricao, a.tipo = :tipo, a.preco =:preco WHERE a.id = :id")
    void salvarEdicao(Long id, String nome, String descricao, String tipo, Double preco);

    @Modifying
    @Query("UPDATE Advice a SET a.quantidadeCompras = :qtdCompra WHERE a.id = :id")
    void adicionarCompra(@Param("qtdCompra") Integer quantidadeCompras, @Param("id") Long id);

}
