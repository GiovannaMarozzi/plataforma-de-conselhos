package com.example.hypeadvice.domain.repository;

import com.example.hypeadvice.domain.entity.Advice;
import com.example.hypeadvice.domain.entity.Compras;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
;

public interface ComprasRepository  extends JpaRepository<Compras, Long> {

    Integer countByConselhoId(Long advice);
}
