package com.example.hypeadvice.domain.bean;

import com.example.hypeadvice.domain.entity.Advice;
import com.example.hypeadvice.domain.service.AdviceService;
import com.example.hypeadvice.domain.vo.AdviceListVO;
import com.example.hypeadvice.domain.vo.AdviceVO;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import java.util.Collections;

@Named
@ViewScoped
public class AdviceListBean extends Bean {

    @Autowired AdviceService adviceService;

    private Advice advice = new Advice();
    private AdviceListVO adviceListVO;


    public void initBean() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().clear();
        advice = new Advice();
        adviceListVO = new AdviceListVO();
        listarTodosConselhos();

    }


    public void buscar() {
        try {
            this.adviceListVO = adviceService.buscar(advice);
        } catch (Exception e) {
            addMessageError(e);
        }
    }

    public void listarTodosConselhos(){
        try{
            this.adviceListVO = adviceService.listarTodos();
        }catch (Exception e){
            addMessageError(e);
        }
    }
    public void buscarPorId() {
        try {
            AdviceVO adviceVO = adviceService.buscarPorId(advice.getId());
            if (adviceVO != null) {
                adviceListVO.setSlips(Collections.singletonList(adviceVO.getSlip()));
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Conselho encontrado com sucesso!", null));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Nenhum conselho encontrado", null));
        }
    }

    public AdviceListVO getAdviceListVO() {
        return adviceListVO;
    }

    public void setAdviceListVO(AdviceListVO adviceListVO) {
        this.adviceListVO = adviceListVO;
    }

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }
}