package com.example.hypeadvice.domain.bean;

import com.example.hypeadvice.domain.entity.Advice;
import com.example.hypeadvice.domain.entity.Compras;
import com.example.hypeadvice.domain.service.AdviceService;
import com.example.hypeadvice.domain.service.ComprasService;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.RequestContext;
import javax.faces.context.PartialViewContext;
import javax.faces.context.FacesContext;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import java.util.List;
import java.util.Objects;
import java.util.Random;

@Named
@ViewScoped
public class AdviceBean extends Bean {

    @Autowired
    AdviceService adviceService;

    private Advice advice = new Advice();

    @Autowired
    ComprasService comprasService;

    private Compras compras = new Compras();
    private Compras conselhoSelecionado;

    private List<Advice> advices;

    public void initBean() {
        advices = adviceService.findAll();
    }

    public List<Advice> getAdvices() {
        return advices;
    }

    public void setAdvices(List<Advice> advices) {
        this.advices = advices;
    }

    private boolean modalVisible;

    public boolean isModalVisible() {
        return modalVisible;
    }

    public void salvar() {
        if (Objects.equals(advice.getTipo(), "") || Objects.equals(advice.getDescricao(), "") || Objects.equals(advice.getNome(), "")) {
            addFaceMessage(FacesMessage.SEVERITY_ERROR, "Não é possível cadastrar um conselho faltando algum de seus atributos", null);

        } else if (Objects.equals(advice.getTipo(), "Pago") && Objects.equals(advice.getPreco(), null)) {
            addFaceMessage(FacesMessage.SEVERITY_ERROR, "Cadastre o valor dos conselho", null);

        } else {
            adviceService.save(advice);
            advices.add(advice);
            adicionarAdvice();
            addFaceMessage(FacesMessage.SEVERITY_INFO, "Sucesso", null);
        }
    }

    public void gerar() {
        try {
            advice = adviceService.gerar();
        } catch (UnirestException e) {
            addMessageError(e);
        }
    }

    public void excluir(Advice advice) {
        try {
            Integer quantidadeCompras = comprasService.buscarConselhosPorId(advice.getId());
            if (quantidadeCompras > 0) {
                addFaceMessage(FacesMessage.SEVERITY_ERROR, "Não é possível excluir o conselho, pois existem compras associadas a ele.", null);
                return;
            }

            adviceService.excluir(advice.getId());
            advices.remove(advice);
            addFaceMessage(FacesMessage.SEVERITY_INFO, "Conselho excluído com sucesso", null);
        } catch (Exception e) {
            addFaceMessage(FacesMessage.SEVERITY_ERROR, "Erro ao excluir conselho", null);
        }
    }

    public void atualizar() {
        try {
            adviceService.atualizar(advice);
            addFaceMessage(FacesMessage.SEVERITY_INFO, "Conselho atualizado com sucesso", null);

            FacesContext facesContext = FacesContext.getCurrentInstance();
            PartialViewContext partialViewContext = facesContext.getPartialViewContext();

            partialViewContext.getRenderIds().add("form-advice-list:amodal:modalTable");
        } catch (Exception e) {
            addFaceMessage(FacesMessage.SEVERITY_ERROR, "Erro ao atualizar conselho", null);
        }
    }

    public void showEditModal(Advice advice) {
        this.advice = advice;
    }

    public void comprar_adquirir() {
        this.conselhoSelecionado = new Compras();
        try {
            conselhoSelecionado.setCliente(compras.getCliente());
            comprasService.salvarCompra(advice, conselhoSelecionado);
            adviceService.atualizarQtd(advice);
            conselhoSelecionado = null;

            addFaceMessage(FacesMessage.SEVERITY_INFO, "Conselho comprado com sucesso", null);
        } catch (Exception e) {
            addFaceMessage(FacesMessage.SEVERITY_ERROR, "Erro ao comprar ou adquirir conselho", null);
        }
    }


    public void adicionarAdvice() {
        advice = new Advice();
    }

    public AdviceBean() {
        advice = new Advice();
        mostrarAccordion = false;
    }

    public void handleTipoConselhoChange() {
        if (advice.getTipo().equals("Pago")) {
            mostrarAccordion = true;
        } else {
            mostrarAccordion = false;
        }
    }

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }

    public boolean isMostrarAccordion() {
        return mostrarAccordion;
    }

    public void setMostrarAccordion(boolean mostrarAccordion) {
        this.mostrarAccordion = mostrarAccordion;
    }

    private boolean mostrarAccordion;

    public Compras getCompras() {
        return compras;
    }

    public void setCompras(Compras compras) {
        this.compras = compras;
    }
}
